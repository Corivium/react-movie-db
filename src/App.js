import React from 'react';
import './App.css';
import { MovieCard } from './components/MovieCard';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MovieDialog from './components/MovieDialog';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';



const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

class App extends React.Component {

  constructor() {
    super();

    this.state = {
      movies: [],
      selectedMovie: null,
      searchText: ''
    };
  }

  selectMovie = movie => this.setState({
    selectedMovie: movie
  });

  clearMovie = () => this.setState({
    selectMovie: null
  })

  searchTextChanged = e => this.setState({ searchText: e.target.value });

  search = async e => {
    e.preventDefault();
    const {searchText} = this.state;

    const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=09a471f7486325cee433ade60e34a243&query=${searchText}&language=en-US&page=1`);
    const json = await response.json();
    this.setState({ movies: json.results });
  }

  async componentDidMount() {
    const response = await fetch('https://api.themoviedb.org/3/movie/top_rated?api_key=09a471f7486325cee433ade60e34a243&language=en-US&page=1');
    const json = await response.json();
    this.setState({ movies: json.results });
  }

  render() {

    const {movies, selectedMovie, searchText} = this.state;
    
    return (
      <div>
        <AppBar position="fixed" color="primary">
          <Toolbar>
            <Typography variant="title" color="inherit" className="title">
              Top Movies
            </Typography>
            <form onSubmit={this.search}>
              <Input
                type="search"
                value={searchText}
                onChange={this.searchTextChanged}
              />
            </form>
          </Toolbar>
        </AppBar>
        <div className="movies">
          { movies.map(movie => 
            <MovieCard key={movie.id} movie={movie} selectMovie={this.selectMovie} />
          )}
        </div>
        <MovieDialog movie={selectedMovie} handleClose={this.clearMovie} />
      </div>
    );

  }
}


export default App;
